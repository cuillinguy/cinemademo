USE [master]
GO
/****** Object:  Database [Cinema]    Script Date: 01/03/2021 13:33:17 ******/
CREATE DATABASE [Cinema]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Cinema', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Cinema.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Cinema_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\Cinema_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Cinema] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Cinema].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Cinema] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Cinema] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Cinema] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Cinema] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Cinema] SET ARITHABORT OFF 
GO
ALTER DATABASE [Cinema] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Cinema] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Cinema] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Cinema] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Cinema] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Cinema] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Cinema] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Cinema] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Cinema] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Cinema] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Cinema] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Cinema] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Cinema] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Cinema] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Cinema] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Cinema] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Cinema] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Cinema] SET RECOVERY FULL 
GO
ALTER DATABASE [Cinema] SET  MULTI_USER 
GO
ALTER DATABASE [Cinema] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Cinema] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Cinema] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Cinema] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Cinema] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Cinema', N'ON'
GO
ALTER DATABASE [Cinema] SET QUERY_STORE = OFF
GO
USE [Cinema]
GO
/****** Object:  Table [dbo].[Bookings]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bookings](
	[BookingId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_Bookings] PRIMARY KEY CLUSTERED 
(
	[BookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Films]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Films](
	[FilmId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Films] PRIMARY KEY CLUSTERED 
(
	[FilmId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SeatBookings]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeatBookings](
	[SeatBookingId] [int] IDENTITY(1,1) NOT NULL,
	[TheatreViewId] [int] NOT NULL,
	[SeatId] [int] NOT NULL,
	[Booked] [bit] NOT NULL,
	[BookingId] [int] NOT NULL,
 CONSTRAINT [PK_SeatBookings] PRIMARY KEY CLUSTERED 
(
	[SeatBookingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Theatres]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Theatres](
	[TheatreId] [int] IDENTITY(1,1) NOT NULL,
	[TheatreName] [nvarchar](max) NULL,
 CONSTRAINT [PK_Theatres] PRIMARY KEY CLUSTERED 
(
	[TheatreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TheatreSeats]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TheatreSeats](
	[TheatreSeatId] [int] IDENTITY(1,1) NOT NULL,
	[SeatNumber] [int] NOT NULL,
	[TheatreId] [int] NOT NULL,
 CONSTRAINT [PK_TheatreSeats] PRIMARY KEY CLUSTERED 
(
	[TheatreSeatId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TheatreViews]    Script Date: 01/03/2021 13:33:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TheatreViews](
	[TheatreViewId] [int] IDENTITY(1,1) NOT NULL,
	[TheatreId] [int] NOT NULL,
	[FilmId] [int] NOT NULL,
	[StartTime] [datetime2](7) NOT NULL,
	[EndTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TheatreViews] PRIMARY KEY CLUSTERED 
(
	[TheatreViewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bookings] ON 

INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (1, 493)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (2, 1086)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (3, 768)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (4, 295)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (5, 1399)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (6, 1125)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (7, 409)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (8, 195)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (9, 1727)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (10, 620)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (11, 1152)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (12, 1365)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (13, 1896)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (14, 799)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (15, 320)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (16, 670)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (17, 475)
INSERT [dbo].[Bookings] ([BookingId], [UserId]) VALUES (18, 263)
SET IDENTITY_INSERT [dbo].[Bookings] OFF
SET IDENTITY_INSERT [dbo].[Films] ON 

INSERT [dbo].[Films] ([FilmId], [Name]) VALUES (1, N'Star Wars')
SET IDENTITY_INSERT [dbo].[Films] OFF
SET IDENTITY_INSERT [dbo].[SeatBookings] ON 

INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (1, 1, 1, 0, 17)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (2, 1, 2, 0, 14)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (3, 1, 3, 0, 15)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (4, 1, 4, 0, 16)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (5, 1, 5, 0, 12)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (9, 2, 1, 0, 18)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (10, 2, 2, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (11, 2, 3, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (12, 2, 4, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (13, 2, 5, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (14, 3, 1, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (15, 3, 2, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (16, 3, 3, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (17, 3, 4, 0, 0)
INSERT [dbo].[SeatBookings] ([SeatBookingId], [TheatreViewId], [SeatId], [Booked], [BookingId]) VALUES (18, 3, 5, 0, 0)
SET IDENTITY_INSERT [dbo].[SeatBookings] OFF
SET IDENTITY_INSERT [dbo].[Theatres] ON 

INSERT [dbo].[Theatres] ([TheatreId], [TheatreName]) VALUES (1, N'Vue Main Theatre')
SET IDENTITY_INSERT [dbo].[Theatres] OFF
SET IDENTITY_INSERT [dbo].[TheatreSeats] ON 

INSERT [dbo].[TheatreSeats] ([TheatreSeatId], [SeatNumber], [TheatreId]) VALUES (1, 1, 1)
INSERT [dbo].[TheatreSeats] ([TheatreSeatId], [SeatNumber], [TheatreId]) VALUES (2, 2, 1)
INSERT [dbo].[TheatreSeats] ([TheatreSeatId], [SeatNumber], [TheatreId]) VALUES (3, 3, 1)
INSERT [dbo].[TheatreSeats] ([TheatreSeatId], [SeatNumber], [TheatreId]) VALUES (4, 4, 1)
SET IDENTITY_INSERT [dbo].[TheatreSeats] OFF
SET IDENTITY_INSERT [dbo].[TheatreViews] ON 

INSERT [dbo].[TheatreViews] ([TheatreViewId], [TheatreId], [FilmId], [StartTime], [EndTime]) VALUES (1, 1, 1, CAST(N'2021-02-28T13:00:00.0000000' AS DateTime2), CAST(N'2021-02-28T15:00:00.0000000' AS DateTime2))
INSERT [dbo].[TheatreViews] ([TheatreViewId], [TheatreId], [FilmId], [StartTime], [EndTime]) VALUES (2, 1, 1, CAST(N'2021-02-28T15:00:00.0000000' AS DateTime2), CAST(N'2021-02-28T17:00:00.0000000' AS DateTime2))
INSERT [dbo].[TheatreViews] ([TheatreViewId], [TheatreId], [FilmId], [StartTime], [EndTime]) VALUES (3, 1, 1, CAST(N'2021-02-28T17:00:00.0000000' AS DateTime2), CAST(N'2021-02-28T19:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[TheatreViews] OFF
USE [master]
GO
ALTER DATABASE [Cinema] SET  READ_WRITE 
GO

# README #

CinemaDemo is a DotNet Core MVC Application. It Uses Entity Framework to connect to a SQL Server database.

### Setup ###

* Version 1.0.0.0

### How do I get set up? ###

* Use SQL Script- ScriptAndData.sql
* Dependencies - SQL Server
* There is also a completed backup file: CinemaBackup.bak

### Who do I talk to? ###

* Richard Lloyd
* Cuuillinguy@outlook.com
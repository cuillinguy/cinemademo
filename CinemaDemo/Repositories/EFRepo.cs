﻿using CinemaDemo.Data;
using CinemaDemo.Models;
using CinemaDemo.Models.PresentationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CinemaDemo.Repositories
{
    public class EFRepo<T> : IEFRepo<FilmListingModel>
        where T : class
    {

        //private readonly CinemaContext _context;

        private readonly CinemaContext _context;

        public EFRepo(CinemaContext context)
        {
            _context = context;

        }

        /// <summary>
        /// Get Film Showings
        /// A cinema has a number of Theatres (Screens) that show films
        /// Each Theatre (Screen) has a number of possible viewings (TheatreView table)
        /// TODO: Real World - would choose a more robust exception handling/logging approach
        /// </summary>
        public IEnumerable<FilmListingModel> GetFilmShowings()
        {

            try
            {
                using(_context)
                {
                    //Join theatres

                    var filmShowings = (from tv in _context.TheatreViews
                                            join t in _context.Theatres
                                            on tv.TheatreId equals t.TheatreId
                                            join f in _context.Films
                                            on tv.FilmId equals f.FilmId
                                        select new
                                        {
                                           Id = tv.TheatreId,
                                           ViewingId = tv.TheatreViewId,
                                           Start = tv.StartTime,
                                           End = tv.EndTime,
                                           Screen = t.TheatreName,
                                           Film = f.Name
                                        });

                    var result = new List<FilmListingModel>();
                    //Bind to a Model

                    foreach (var showing in filmShowings)
                    {
                        var listingModel = new FilmListingModel()
                        {
                            Id = showing.Id,
                            ViewingId = showing.ViewingId,
                             Start = showing.Start,
                              End = showing.End,
                               Film = showing.Film,
                                Screen = showing.Screen
                        };

                        result.Add(listingModel);

                    }

                   
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Get a single screening
        /// We pre-populate the possible seats available for each viewing slot 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IEnumerable<FilmListingModel> GetFilmListingById(int Id)
        {
            try
            {
                using (_context)
                {
                    var filmShowings = (from tv in _context.TheatreViews
                                        join t in _context.Theatres
                                        on tv.TheatreId equals t.TheatreId
                                        join f in _context.Films
                                        on tv.FilmId equals f.FilmId
                                        where tv.TheatreViewId == Id
                                        select new
                                        {
                                            Id = tv.TheatreId,
                                            ViewingId = tv.TheatreViewId,
                                            Start = tv.StartTime,
                                            End = tv.EndTime,
                                            Screen = t.TheatreName,
                                            Film = f.Name
                                        });

                    var result = new List<FilmListingModel>();
                    //Bind to a Model

                    foreach (var showing in filmShowings)
                    {
                        var listingModel = new FilmListingModel()
                        {
                            Id = showing.Id,
                            ViewingId = showing.ViewingId,
                            Start = showing.Start,
                            End = showing.End,
                            Film = showing.Film,
                            Screen = showing.Screen
                        };

                        //Seats for this viewing
                        var seatsAvailable = (from sb in _context.SeatBookings
                                              where sb.Booked == false && sb.TheatreViewId == Id
                                              select new
                                              {
                                                  SeatId = sb.SeatId
                                              });

                        foreach (var seat in seatsAvailable)
                        {

                            listingModel.SeatsAvailableList.Add(seat.SeatId);
                        }


                        result.Add(listingModel);

                    }

                    return result.ToList();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Create a booking
        /// </summary>
        /// <param name="filmListingModel"></param>
        /// <returns></returns>
        public bool BookSeats(FilmListingModel filmListingModel, int UserId)
        {
            var result = false;

            
                //Add Booking
                using (_context)
                {
                    using (var trans = _context.Database.BeginTransaction())
                    {

                        try
                        {
                            var booking = new Booking();
                            booking.UserId = UserId;
                            _context.Add(booking);
                            _context.SaveChanges();

                            var bookingId = booking.BookingId;

                            foreach (var seatId in filmListingModel.SeatsAvailableList)
                            {

                                //Get this row in the seatc booking table
                                var seat = _context.SeatBookings.FirstOrDefault(x => x.SeatId == seatId && x.TheatreViewId == filmListingModel.ViewingId);
                                seat.BookingId = bookingId;
                                seat.Booked = true;
                                _context.SaveChanges();
                            }


                        trans.Commit();

                        result = true;
                    }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            
                            throw ex;
                        }
                    }


                }


            

            return result;
        }
    }
}

﻿using CinemaDemo.Data;
using CinemaDemo.Models.PresentationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Repositories
{
    public interface IEFRepo<T> where T : class
    {
        public IEnumerable<T> GetFilmShowings();
        public IEnumerable<T> GetFilmListingById(int Id);
        public bool BookSeats(FilmListingModel filmListingModel, int UserId);

    }
}

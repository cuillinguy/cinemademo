﻿using CinemaDemo.Models.PresentationModels;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CinemaDemo.Repositories
{
    public class SqlRepo : ISqlRepo
    {

        private readonly string _connectionString;

        public SqlRepo(string ConnectionString)
        {
            _connectionString = ConnectionString;
        }

        public List<FilmListingModel> GetFilmShowings()
        {
            var result = new List<FilmListingModel>();

            using(SqlConnection sql = new SqlConnection(_connectionString))
            {
                using(SqlCommand cmd = new SqlCommand("getTheatreViews", sql))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    sql.Open();

                    using(var reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {

                            result.Add(MapFilmListingModel(reader));


                        }
                    }
                }

            }

            return result;
        }

        private FilmListingModel MapFilmListingModel(SqlDataReader reader)
        {
            return new FilmListingModel()
            {
                Id = (int)reader["theatreid"],
                 ViewingId = (int)reader["TheatreViewId"],
                  Start = (DateTime)reader["StartTime"],
                   End = (DateTime)reader["EndTime"],
                    Screen = (string)reader["TheatreName"],
                     Film = (string)reader["Name"]
            };
        }

        public List<FilmListingModel> GetFilmListingById(int Id)
        {
            var result = new List<FilmListingModel>();

            return result;
        }

        public bool BookSeats(FilmListingModel filmListingModel, int UserId)
        {
            return false;
        }
    }
}

﻿using CinemaDemo.Models.PresentationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Repositories
{
    public interface ISqlRepo
    {
        public List<FilmListingModel> GetFilmShowings();

        public List<FilmListingModel> GetFilmListingById(int Id);

        public bool BookSeats(FilmListingModel filmListingModel, int UserId);
    }
}

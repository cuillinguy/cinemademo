﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Models.EmployeeModels
{
    public class Contractor : IEmployed
    {
        public int EmpId { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaDemo.Models
{
    public class TheatreView
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TheatreViewId { get; set; }

        public int TheatreId { get; set; }

        public int FilmId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaDemo.Models
{
    public class TheatreSeat
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TheatreSeatId { get; set; }

        /// <summary>
        /// The number on the seat
        /// </summary>
        public int SeatNumber { get; set; }

        public int TheatreId { get; set; }
    }
}

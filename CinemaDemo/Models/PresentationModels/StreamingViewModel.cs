﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Models.PresentationModels
{
    public class StreamingViewModel
    {
        public List<string> FilmsListModel { get; set; }

        public StreamingViewModel() {

            this.FilmsListModel = new List<string>();
        }
    }
}

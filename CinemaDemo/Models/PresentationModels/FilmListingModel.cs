﻿using CinemaDemo.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Models.PresentationModels
{
    public class FilmListingModel : IEntity
    {
        public int Id { get; set; }
        public int ViewingId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Screen { get; set; }
        public string Film { get; set; }

        public int SeatsBooking { get; set; }

        public int UserId { get; set; }

        public List<int> SeatsAvailableList { get; set; }

        public FilmListingModel()
        {
            SeatsAvailableList = new List<int>();
        }

    }
}

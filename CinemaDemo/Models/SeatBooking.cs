﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaDemo.Models
{
    public class SeatBooking
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SeatBookingId { get; set; }

        public int TheatreViewId { get; set; }

        public int SeatId { get; set; }

        public bool Booked { get; set; }

        public int BookingId { get; set; }


    }
}

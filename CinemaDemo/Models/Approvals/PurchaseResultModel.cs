﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Models.Approvals
{
    public class PurchaseResultModel
    {
        public string ResultMessage { get; set; }

        public Purchase Purchase { get; set; }
    }
}

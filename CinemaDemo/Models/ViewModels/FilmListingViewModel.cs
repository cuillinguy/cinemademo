﻿using CinemaDemo.Models.PresentationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Models.ViewModels
{
    public class FilmListingViewModel
    {
        public List<FilmListingModel> FilmListings { get; set; }
    }
}

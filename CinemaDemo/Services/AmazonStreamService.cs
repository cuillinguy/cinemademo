﻿using CinemaDemo.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Services
{
    public class AmazonStreamService : IStreamService
    {
        public List<string> ShowMovies()
        {
            return new List<string>()
            {
                "Movie A",
                "Movie B",
                "Movie C"
            };
        }
    }
}

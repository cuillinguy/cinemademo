﻿using CinemaDemo.Data;
using CinemaDemo.Factories;
using CinemaDemo.Models;
using CinemaDemo.Models.PresentationModels;
using CinemaDemo.Models.ViewModels;
using CinemaDemo.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private TheatreBookingsFactory factory { get; set; }

        private IEFRepo<FilmListingModel> _repo = null;


        private readonly CinemaContext _context;

        /// <summary>
        /// Default Constructor
        /// Real World would add the wiring for DI for the Factory class
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="context"></param>
        public HomeController(ILogger<HomeController> logger, CinemaContext context, IEFRepo<FilmListingModel> repo)
        {
            _context = context;
            _logger = logger;
            factory = new TheatreBookingsFactory(context,repo);
            _repo = repo;
        }

        /// <summary>
        /// The Default View
        /// Shows the Listings
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {

         var filmListings = _repo.GetFilmShowings();

            var viewModel = new FilmListingViewModel();
            viewModel.FilmListings = filmListings.ToList();

            //var factoryModel = factory.GetFilmShowings();

            return View(viewModel);
        }


        /// <summary>
        /// GET: HomeController/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Details(int id)
        {

            var viewModel = factory.GetFilmShowingById(id);

            var status = TempData["Result"];
            ViewBag.Result = status;

            return View(viewModel);
        }

        /// <summary>
        /// Book the seats needed
        /// </summary>
        /// <param name="formData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BookSeats(FilmListingModel formData)
        {
            Random rnd = new Random();
            //Fake the UserID
            //TODO:Real world would have some authentication etc.
            var UserId = rnd.Next(1, 2000);

            var status = "NotSubmitted";

            var result = factory.BookSeats(formData, UserId);

            if (result)
            {
                status = "Success";
            }

            TempData["Result"] = status;
            ViewBag.Result = status;
 
            return RedirectToAction(nameof(Details), new { id = formData.Id });

        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

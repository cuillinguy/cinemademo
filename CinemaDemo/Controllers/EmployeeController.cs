﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaDemo.Models.EmployeeModels;

namespace CinemaDemo.Controllers
{
    /// <summary>
    /// Composite Design Pattern
    /// </summary>
    public class EmployeeController : Controller
    {
        /// <summary>
        /// Employee Controller
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            Employee Rahul = new Employee { EmpId = 1, Name = "Rahul" };//The manager
            Employee Amit = new Employee { EmpId = 2, Name = "Amit" };
            Employee Mohan = new Employee { EmpId = 3, Name = "Mohan" };

            Rahul.AddSubordinate(Amit);
            Rahul.AddSubordinate(Mohan);

            Employee Rita = new Employee { EmpId = 4, Name = "Rita" };
            Employee Hari = new Employee { EmpId = 5, Name = "Hari" };

            Amit.AddSubordinate(Rita);
            Amit.AddSubordinate(Hari);

            Employee Kamal = new Employee { EmpId = 6, Name = "Kamal" };
            Employee Raj = new Employee { EmpId = 7, Name = "Raj" };

            Contractor Sam = new Contractor { EmpId = 8, Name = "Sam" };
            Contractor tim = new Contractor { EmpId = 9, Name = "Tim" };

            Mohan.AddSubordinate(Kamal);
            Mohan.AddSubordinate(Raj);
            Mohan.AddSubordinate(Sam);
            Mohan.AddSubordinate(tim);

            foreach (Employee manager in Rahul)
            {
                var topManager = manager;

                foreach (var employee in manager)
                {
                    var subordinate = employee;

                }
            }


            return View();
        }

        // GET: EmployeeController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

      
        // POST: EmployeeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
      
    }
}

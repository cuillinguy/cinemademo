﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaDemo.Models.Approvals;

namespace CinemaDemo.Controllers
{
    /// <summary>
    /// Chain of responsibility pattern
    /// https://www.dofactory.com/net/chain-of-responsibility-design-pattern
    /// </summary>
    public class ApprovalsController : Controller
    {
        
        public ActionResult Index()
        {
            Approver directorLarry = new Director(); //<10000K
            Approver vicePresidentSam = new VicePresident();//<25000
            Approver presidentTammy = new President();//<100000

            //Set the chain
            directorLarry.SetSuccessor(vicePresidentSam);
            vicePresidentSam.SetSuccessor(presidentTammy);

            // Generate and process purchase requests
            Purchase p1 = new Purchase(2034, 350.00, "Assets");
            directorLarry.ProcessRequest(p1);

            Purchase p2 = new Purchase(2035, 32590.10, "Project X");
            directorLarry.ProcessRequest(p2);

            Purchase p3 = new Purchase(2036, 122100.00, "Project Y");
            directorLarry.ProcessRequest(p3);

            Purchase p4 = new Purchase(2037, 122100.00, "Project Z");
            presidentTammy.ProcessRequest(p4);

            return View();
        }

        // GET: ApprovalsController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ApprovalsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ApprovalsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ApprovalsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ApprovalsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ApprovalsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}

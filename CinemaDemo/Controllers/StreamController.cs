﻿using CinemaDemo.Factories;
using CinemaDemo.Models.PresentationModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Controllers
{
    public class StreamController : Controller
    {
        private readonly IStreamFactory streamFactory;

        public StreamController(IStreamFactory streamFactory)
        {
            this.streamFactory = streamFactory;
        }

        public IEnumerable<string> GetMovies(string userSelection)
        {
            return streamFactory.GetStreamService(userSelection).ShowMovies();
        }

        [HttpGet("movies/{userSelection}")]
        public IActionResult Index(string userSelection)
        {
            var viewModel = new StreamingViewModel();
            var filmList = streamFactory.GetStreamService(userSelection).ShowMovies();
            viewModel.FilmsListModel = filmList;

            return View(viewModel);
        }

        [HttpGet]
        public IActionResult GetListings(string userSelection)
        {
            var viewModel = new StreamingViewModel();
            var filmList = streamFactory.GetStreamService(userSelection).ShowMovies();
            viewModel.FilmsListModel = filmList;

            return View("Index", viewModel);
        }
    }
}

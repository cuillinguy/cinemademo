﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaDemo.Models;
using Microsoft.EntityFrameworkCore;

namespace CinemaDemo.Data
{
    public class CinemaContext : DbContext
    {
        public CinemaContext(DbContextOptions<CinemaContext> options) : base(options) { }

        //Initialize Models here
        public DbSet<Theatre> Theatres { get; set; }
        public DbSet<TheatreView> TheatreViews { get; set; }
        public DbSet<TheatreSeat> TheatreSeats { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<SeatBooking> SeatBookings { get; set; }
        public DbSet<Booking> Bookings { get; set; }

        
    }
}

﻿using CinemaDemo.Models.PresentationModels;
using CinemaDemo.Models.ViewModels;
using CinemaDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Factories
{
    public class FilmFactory : IFilmFactory
    {

        private readonly string _connectionString;
        private readonly SqlRepo _sqlRepo;

        public FilmFactory(string ConnectionString)
        {
            _connectionString = ConnectionString;
            _sqlRepo = new SqlRepo(_connectionString);
        }
        public bool BookSeats(FilmListingModel filmListingModel, int UserId)
        {
            throw new NotImplementedException();
        }

        public FilmListingViewModel GetFilmListingById(int Id)
        {
            throw new NotImplementedException();
        }

        public FilmListingViewModel GetFilmShowings()
        {
            try
            {
                var result = new FilmListingViewModel();
                result.FilmListings = _sqlRepo.GetFilmShowings();
                return result;

            }
            catch (Exception)
            {

                throw;
            }
            //throw new NotImplementedException();
        }
    }
}

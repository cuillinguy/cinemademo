﻿using CinemaDemo.Models.PresentationModels;
using CinemaDemo.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Factories
{
    public interface IFilmFactory
    {
        public FilmListingViewModel GetFilmShowings();
        public FilmListingViewModel GetFilmListingById(int Id);

        public bool BookSeats(FilmListingModel filmListingModel, int UserId);
    }
}

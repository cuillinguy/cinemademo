﻿using CinemaDemo.Data;
using CinemaDemo.Models.PresentationModels;
using CinemaDemo.Models.ViewModels;
using CinemaDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Factories
{
    public class TheatreBookingsFactory : ITheatreBookingsFactory
    {
        private readonly CinemaContext _context;
        private readonly IEFRepo<FilmListingModel> _repo = null;

        /// <summary>
        /// Default Constructor
        /// TODO: Real World would add DI to enable passing in of Interface 
        /// and use DotNet Cores DI wiring
        /// </summary>
        /// <param name="context"></param>
        public TheatreBookingsFactory(CinemaContext context, IEFRepo<FilmListingModel> repo)
        {
            _context = context;
            _repo = repo;
        }

        public FilmListingViewModel GetFilmShowings()
        {
            try
            {
                //Create the viewModel
                var result = new FilmListingViewModel();

                //result.FilmListings = _repo.GetFilmShowings();

                return result;

            }
            catch (Exception)
            {

                throw;
            }

        }

        public FilmListingViewModel GetFilmShowingById(int Id)
        {
            try
            {
                var result = new FilmListingViewModel();

                result.FilmListings = (List<FilmListingModel>)_repo.GetFilmListingById(Id);


                return result;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public bool BookSeats(FilmListingModel filmListingModel, int UserId)
        {

            try
            {

              return  _repo.BookSeats(filmListingModel, UserId);

            }
            catch (Exception)
            {

                throw;
            }



            
        }

    }
}

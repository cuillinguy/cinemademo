﻿using CinemaDemo.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Factories
{
    public class StreamFactory : IStreamFactory
    {
        public readonly IServiceProvider serviceProvider;

        public StreamFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public IStreamService GetStreamService(string userSelection)
        {
            if (userSelection == "Netflix")
            {
                return (IStreamService)serviceProvider.GetService(typeof(NetflixStreamService));
            }
            else
            {
                return (IStreamService)serviceProvider.GetService(typeof(AmazonStreamService));
            }
        }
    }
}

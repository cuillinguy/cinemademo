﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaDemo.Factories
{
    public interface IStreamFactory
    {
        public IStreamService GetStreamService(string userSelection);
    }
}

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CinemaDemo.Data;
using Microsoft.EntityFrameworkCore;
using CinemaDemo.Factories;
using CinemaDemo.Repositories;
using CinemaDemo.Models.PresentationModels;
using CinemaDemo.Services;

namespace CinemaDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            //Wires up the EF Database Context
            services.AddDbContext<CinemaContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddScoped<ITheatreBookingsFactory, TheatreBookingsFactory>();
            services.AddScoped(typeof(IEFRepo<>), typeof(EFRepo<>));
            services.AddScoped<StreamFactory>();
            services.AddScoped<NetflixStreamService>()

                .AddScoped<IStreamService, NetflixStreamService>(s => s.GetService<NetflixStreamService>());

            services.AddScoped<AmazonStreamService>()
                .AddScoped<IStreamService, AmazonStreamService>(s => s.GetService<AmazonStreamService>());

            services.AddScoped(typeof(IStreamFactory), typeof(StreamFactory));


            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
